using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Timer : MonoBehaviour
{
    public static float timeStart = 25;
    public Text timerText;


    void Start()
    {
        timeStart = 25;
        timerText.text = timeStart.ToString();    
    }

    void Update()
    {
        timeStart -= Time.deltaTime;
        timerText.text = Mathf.Round(timeStart).ToString();
    }
}
