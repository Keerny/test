[33mcommit 624774d6a8ef3056eba850c3666b1fffafe6943b[m[33m ([m[1;36mHEAD -> [m[1;32mmain[m[33m)[m
Author: Kirushin Yuriy <zxq-1234@mail.ru>
Date:   Fri Mar 25 07:08:14 2022 +0300

    add a sciene

[33mcommit 34b40bc602db22ffa2119de3960de0b442587960[m
Author: Kirushin Yuriy <zxq-1234@mail.ru>
Date:   Thu Mar 24 23:21:55 2022 +0300

    Chang README

[33mcommit 868b957fe37de2a0a5e183d59ce03a179afee932[m[33m ([m[1;31morigin/main[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Merge: 58b3a1e 99ce736
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Mon Mar 7 23:56:39 2022 +0300

    Merge branch 'main' of https://github.com/lenchez-ch/ih-workshop2022-01

[33mcommit 58b3a1e205103079623bd908d6eb8a55618fe455[m
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Mon Mar 7 23:56:09 2022 +0300

    Added time step examples

[33mcommit 99ce736bb4334807f64d5004de63a1e7ce8d3b04[m
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Mon Mar 7 15:40:40 2022 +0300

    Name changed in README

[33mcommit 5327e1cc10f97a6f9b51665aca14a86d4ae6f4b5[m
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Mon Mar 7 15:19:11 2022 +0300

    Added basic README.md

[33mcommit 4f0174ac35b2b423e65281fb0095b9d67dff61b9[m
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Thu Mar 3 23:33:27 2022 +0300

    Added level 2 stub

[33mcommit 369b56bef91e60cd58cd382b8f75dded232c199e[m
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Thu Mar 3 01:54:37 2022 +0300

    Lava & loss

[33mcommit d89b4ca6c6b2a4a3cd77606348248a009f51a8d0[m
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Wed Mar 2 01:00:04 2022 +0300

    Goal object & victory UI + crouch fix

[33mcommit a0a7f5c2f2df8da53322c39d6cc364850d037336[m
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Mon Feb 28 23:41:50 2022 +0300

    Added camera trailing, tweaked midair movement

[33mcommit 1415f49839c3206b5a1ec6c91566d2ce3132a705[m
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Mon Feb 28 00:22:14 2022 +0300

    Added basic player movement

[33mcommit 34820e19960dbcee9a709c7f9275b7b280f51d2d[m
Author: Alexey Chubar <lenchez@forelock.local>
Date:   Sat Feb 26 11:06:03 2022 +0300

    Created unity project
